package net.chiragaggarwal.awesomealarm;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Alarm {
    public static final SimpleDateFormat ALARM_DATE_FORMAT = new SimpleDateFormat("hh:mm");
    public static final SimpleDateFormat ALARM_MERIDIEM_FORMAT = new SimpleDateFormat("aa");
    private final Date date;


    public Alarm(long timestampToRingAt) {
        this.date = new Date(timestampToRingAt);

    }

    public String visibleTime() {
        return ALARM_DATE_FORMAT.format(date);
    }

    public String visibleMeridiem() {
        return ALARM_MERIDIEM_FORMAT.format(date);
    }
}
