package net.chiragaggarwal.awesomealarm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class AlarmActivity extends AppCompatActivity {
    private RecyclerView listAlarms;
    private AlarmsAdapter alarmsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        FloatingActionButton fabAddAlarm = (FloatingActionButton) findViewById(R.id.fab_add_alarm);
        fabAddAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        setupAlarmsList();
    }

    private void setupAlarmsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listAlarms = (RecyclerView) findViewById(R.id.list_alarms);
        listAlarms.setLayoutManager(linearLayoutManager);
        alarmsAdapter = new AlarmsAdapter(this);
        listAlarms.setAdapter(alarmsAdapter);
    }
}
