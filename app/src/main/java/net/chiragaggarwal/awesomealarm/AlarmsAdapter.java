package net.chiragaggarwal.awesomealarm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class AlarmsAdapter extends RecyclerView.Adapter<AlarmViewHolder> {
    private final List<Alarm> alarms;
    private Context context;

    public AlarmsAdapter(Context context) {
        this.context = context;
        alarms = new ArrayList<>();
    }

    @Override
    public AlarmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View alarmView = LayoutInflater.from(context).inflate(R.layout.item_alarm, parent, false);
        return new AlarmViewHolder(alarmView);
    }

    @Override
    public void onBindViewHolder(AlarmViewHolder holder, int position) {
        Alarm alarm = alarms.get(position);
        holder.setTime(alarm.visibleTime());
        holder.setMeridiem(alarm.visibleMeridiem());
    }

    @Override
    public int getItemCount() {
        return alarms.size();
    }

    public void populate(Alarm alarm) {
        alarms.add(alarm);
        notifyDataSetChanged();
    }
}
