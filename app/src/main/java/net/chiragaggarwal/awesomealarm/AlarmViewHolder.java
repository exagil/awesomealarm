package net.chiragaggarwal.awesomealarm;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class AlarmViewHolder extends RecyclerView.ViewHolder {
    private final TextView textAlarmTime;
    private final TextView textMeridiem;

    public AlarmViewHolder(View alarmView) {
        super(alarmView);
        textAlarmTime = (TextView) alarmView.findViewById(R.id.text_alarm_time);
        textMeridiem = (TextView) alarmView.findViewById(R.id.text_meridiem);
    }

    public void setTime(String visibleTime) {
        textAlarmTime.setText(visibleTime);
    }

    public void setMeridiem(String visibleMeridiem) {
        textMeridiem.setText(visibleMeridiem);
    }
}
